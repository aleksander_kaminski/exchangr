
$(document).ready(function(){
    setTimeout(function(){
        $('.topLoader').hide();
    },600);
    
    var welcomeScreen = $("#welcomeScreen");
    var welcomeMessage = $("#welcomeScreen").find(".message");
    
    $.when(welcomeScreen.fadeIn(600)).done( function() {
        welcomeScreen.find('.col-sm-4').css({'filter': 'blur(10px)','transition': 'all 0.5s ease'});
        welcomeMessage.fadeIn(1500);
    });
});

        
$(document).on("scroll", function(e){
   if($("#welcomeScreen").is(':visible')){
       window.scrollTo(0, 0);
       $("#welcomeScreen").fadeOut();
   } else {
       
   }
});
$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
      isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed === true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});

$(document).ready(function () {
   $('.recommendedSingle').on('mouseenter', function(){
      $(this).find('.recommendedTitle').fadeIn();
      $(this).find('img').animate({
         opacity: 0.5
     }, 300, function(){
     });
   });
   $('.recommendedSingle').on('mouseleave', function(){
      $(this).find('.recommendedTitle').fadeOut();
      $(this).find('img').animate({
         opacity: 1
     }, 300, function(){
     });
    });
});

$(function() {
  $('input').on('change', function() {
    var input = $(this);
    if (input.val().length) {
      input.addClass('populated');
    } else {
      input.removeClass('populated');
    }
  });
  
  setTimeout(function() {
    $('#fname').trigger('focus');
  }, 500);
});


$(".modal").on('click', 'button', function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
})
